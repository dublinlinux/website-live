var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

let script = document.createElement('script')
script.innerHTML = `
tailwind.config = {
    darkMode: 'class',
    theme: {
        extend: {
            colors: {
                main_grey: '#232323',
                darker_grey: '#333333',
                off_white: '#EBEBEB',
                nav_bar: '#141414',
                lighter_grey: '#333333',
                myst_main: '#d94d49',
                myst_dark: '#8c312f',
                gzlight: '#7EAAC8',
                gzdark: '#486A90',
                gzdisc: '#5165F6',
                gzdiscdark: '#3244C9',
                gzredlight: '#FC2A1B',
                warship: '#abf6ff',
                gzreddark: '#B01D00',
            }
        }
    }
}
`
document.head.appendChild(script)

}
/*
     FILE ARCHIVED ON 03:53:45 Dec 01, 2022 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 19:54:24 Aug 20, 2024.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 0.693
  exclusion.robots: 0.026
  exclusion.robots.policy: 0.013
  esindex: 0.014
  cdx.remote: 34.552
  LoadShardBlock: 1016.808 (3)
  PetaboxLoader3.datanode: 519.222 (6)
  PetaboxLoader3.resolve: 5396.244 (3)
  load_resource: 5446.41 (2)
  loaddict: 52.251
*/